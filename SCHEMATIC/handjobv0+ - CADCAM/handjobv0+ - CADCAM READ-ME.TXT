LABCENTER PROTEUS TOOL INFORMATION FILE
=======================================

In case of difficulty, please e-mail support@labcenter.co.uk

Tool set up for Proteus layout 'handjobv0+.pdsprj'.
CADCAM generated at 3:01:26 AM on Tuesday, August 15, 2017.

File List
---------
Bottom Copper           : handjobv0+ - CADCAM Bottom Copper.GBR
Drill                   : handjobv0+ - CADCAM Drill.DRL
Netlist                 : handjobv0+ - CADCAM Netlist.IPC

Photoplotter Setup
------------------
Format: RS274X, ASCII, 4.3, metric, absolute, eob=*, LZO
Notes:  D=Diameter, S=Side, W=Width, H=Height, C=Chamfer, I=Index

D10	CIRCLE  D=0.635mm                                                         DRAW 
D11	CIRCLE  D=0.254mm                                                         DRAW 
D12	CIRCLE  D=0.381mm                                                         DRAW 
D13	CIRCLE  D=0.762mm                                                         DRAW 
D14	CIRCLE  D=0.508mm                                                         DRAW 
D15	CIRCLE  D=2.032mm                                                         FLASH
D16	RECT    W=1.016mm             H=1.27mm                                    FLASH
D17	RECT    W=1.27mm              H=1.016mm                                   FLASH
D18	SQUARE  S=1.651mm                                                         FLASH
D19	RECT    W=2.3mm               H=3mm                                       FLASH
D70	SQUARE  S=2.032mm                                                         FLASH
D71	RECT    W=2.159mm             H=0.9398mm                                  FLASH
D20	RECT    W=2.159mm             H=3.2512mm                                  FLASH
D21	CIRCLE  D=0.2032mm                                                        DRAW 

NC Drill Setup
--------------
Format: Excellon, ASCII, 4.3, metric, absolute, eob=<CR><LF>, no zero suppression.
Notes:  Tool sizes are diameters. Layer sets are in brackets - 0=TOP, 15=BOTTOM, 1-14=INNER.

T01	1.27mm (0-15) Plated
T02	0.762mm (0-15) Plated
T03	1.778mm (0-15) Plated
T04	1.016mm (0-15) Plated


[END OF FILE]
