################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../LIB/ADC.cpp \
../LIB/PWM.cpp \
../LIB/UART.cpp 

C_SRCS += \
../LIB/HandJob.c 

C_DEPS += \
./LIB/HandJob.d 

OBJS += \
./LIB/ADC.o \
./LIB/HandJob.o \
./LIB/PWM.o \
./LIB/UART.o 

CPP_DEPS += \
./LIB/ADC.d \
./LIB/PWM.d \
./LIB/UART.d 

OBJS__QUOTED += \
"LIB\ADC.o" \
"LIB\HandJob.o" \
"LIB\PWM.o" \
"LIB\UART.o" 

C_DEPS__QUOTED += \
"LIB\HandJob.d" 

CPP_DEPS__QUOTED += \
"LIB\ADC.d" \
"LIB\PWM.d" \
"LIB\UART.d" 

CPP_SRCS__QUOTED += \
"../LIB/ADC.cpp" \
"../LIB/PWM.cpp" \
"../LIB/UART.cpp" 

C_SRCS__QUOTED += \
"../LIB/HandJob.c" 


