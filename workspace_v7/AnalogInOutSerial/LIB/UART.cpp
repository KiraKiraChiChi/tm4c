/*
 * UART.cpp
 *
 *  Created on: 23 thg 7, 2017
 *      Author: Kira Kira
 */


/*
 * UART.c
 *
 *  Created on: 22 thg 7, 2017
 *      Author: Kira Kira
 */

/*
 * Uart.c
 *
 *  Created on: 18-07-2017
 *      Author: Kira Kira
 */
#include "include.h"
#include "UART.h"


void UART_config()
{
    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
        // Kich hoat uart0
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
        //Cau hinh chan PA0 va PA1 lan luot la chan RX va TX
        GPIOPinConfigure(GPIO_PIN_0);
        GPIOPinConfigure(GPIO_PIN_1);
        GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);  //Thieu dinh nghia pin pa1 pa0 la chan cua UART
        //Cau hinh uart 0 baud 9600
        UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 9600, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

    }

void UART_SendChar(char c)
{
    UARTCharPut(UART0_BASE, c);
    if(UARTCharsAvail(UART0_BASE))
                UARTCharPut(UART0_BASE, UARTCharGet(UART0_BASE));
    }

void UART_SendString(char *c)
{
    while(*c){
        UART_SendChar(*c);
        c++;
      }
    }

char UART_GetChar()
{
    while((UART0_FR_R&UART_FR_RXFE) != 0);
     return((char)(UART0_DR_R&0xFF));
    }

void UART_GetString(char *bufPt, uint16_t max) {
int length=0;
char c;
  c = UART_GetChar();
  while(c != 13){
    if(c == 8){
      if(length){
        bufPt--;
        length--;
        UART_SendChar(8);
      }
    }
    else if(length < max){
      *bufPt = c;
      bufPt++;
      length++;
      UART_SendChar(c);
    }
    c = UART_GetChar();
  }
  *bufPt = 0;
}
