/*
 * PWM.cpp
 *
 *  Created on: 23 thg 7, 2017
 *      Author: Kira Kira
 */

/*
 * PWM.c
 *
 *  Created on: Apr 12, 2016
 *      Author: Satu
 */




#include "include.h"
#include "PWM.h"

static uint32_t  Period;
static uint8_t DutyCycle_Red=5, DutyCycle_Blue=5;

void Config_PWM(void)
{

    Period= SysCtlClockGet()/20000;
    //F0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_0);

    SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    GPIOPinConfigure(GPIO_PF0_M1PWM4);

    PWMGenConfigure(PWM1_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_2, Period);
    PWMGenEnable(PWM1_BASE, PWM_GEN_2);

    //F1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1);

    SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    GPIOPinConfigure(GPIO_PF1_M1PWM5);

    PWMGenConfigure(PWM1_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_2, Period);
    PWMGenEnable(PWM1_BASE, PWM_GEN_2);

    //F2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);

    SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    GPIOPinConfigure(GPIO_PF2_M1PWM6);

    PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, Period);
    PWMGenEnable(PWM1_BASE, PWM_GEN_3);

    //F3
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);
    SysCtlPWMClockSet(SYSCTL_PWMDIV_1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
    GPIOPinConfigure(GPIO_PF3_M1PWM7);

    PWMGenConfigure(PWM1_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN|PWM_GEN_MODE_NO_SYNC);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_2, Period);
    PWMGenEnable(PWM1_BASE, PWM_GEN_2);


}

void Process_PWM(void)
{
    if(DutyCycle_Red==0)
        PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, false);
    else
    {
        PWMPulseWidthSet(PWM1_BASE,PWM_OUT_5, Period*DutyCycle_Red/10-1);
        PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, true);
    }

    if(DutyCycle_Blue==0)
        PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, false);
    else
    {
        PWMPulseWidthSet(PWM1_BASE,PWM_OUT_6, Period*DutyCycle_Blue/10-1);
        PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, true);
    }

}

void Update_PWM(uint8_t *pRed, uint8_t *pBlue)
{
    DutyCycle_Red=*pRed;
    DutyCycle_Blue=*pBlue;
}



