/*
 * PWM.c
 *
 *  Created on: 9 thg 9, 2017
 *      Author: Kira Kira
 */
#include "PWM.h"

void PWM_config()
{

       P2DIR = 0b00011111; // Make P1.0-5 outputs

      // Configure Timer A0 Compare interrupts
      TA0CTL = TASSEL_2 + MC_1 + ID_0; // "up" mode, input divider = 1
      TA0CCR0 = 2500;                  // set timer period to 2.5ms
      TA0CCTL0 = CCIE;                 // enable CC0 interrupt
      TA0CCR1 = 1500;                  // set pulse width to 1.5ms
      TA0CCTL1 = CCIE;                 // enable CC1 interrupt
      _BIS_SR(GIE);                    // global interrupt enable
    }



