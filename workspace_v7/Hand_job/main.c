/*
 * main.c
 */
#include <stdint.h>
#include <stdbool.h>
#include "LIB/include.h"

//#define LED_PERIPH SYSCTL_PERIPH_GPIOF
//#define LED_BASE GPIO_PORTF_BASE
//#define RED_LED GPIO_PIN_1
#define Button_PERIPH SYSCTL_PERIPH_GPIOF
#define ButtonBase GPIO_PORTF_BASE
#define Button GPIO_PIN_4
uint32_t value=0;
volatile uint32_t a[4];
void delay(unsigned int nCount);

//float ChangeValue(float a);

volatile uint32_t ADC[4];  //Khai bao mang 4 phan tu chua gia tri doc duoc tu ADC
int main(void)
 {
   // volatile uint32_t ADC[4]={0};
    //char str[20] = "abc";
    //char str1[5] = {0};
    //volatile uint32_t ui32Avg;
    //volatile uint32_t ui32Avg1;

    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ); //Cau hinh clock he thong
    UART_config();
    //UART_SendString("aaa");
    SysCtlPeripheralEnable(Button_PERIPH);
    GPIOPinTypeGPIOInput(ButtonBase, Button);
    GPIOPadConfigSet(ButtonBase ,Button,GPIO_STRENGTH_8MA,GPIO_PIN_TYPE_STD_WPU);
    value= GPIOPinRead(ButtonBase,Button);
    SysCtlDelay(3);
     ADC_config();
     SysCtlDelay(3);
     while(GPIOPinRead(ButtonBase,Button)!=0 )
        {
        }

    //UART_SendString("abc");
     while(1)
    {
        ADC_GetValue(ADC);
        //sprintf(str1,"%d",ADC[1]);
        a[0] = ChangeValue(ADC[0]);
        a[1] = ChangeValue (ADC[1]);
       while(((a[0]<20)||(a[0]>300))&&(a[1]>50))
       {
       }
        delay(50);
    }
}

void delay(unsigned int nCount)
{
    unsigned int i, j;

    for (i = 0; i < nCount; i++)
        for (j = 0; j < 0x2AFF; j++);
}


/*void main(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ); //40Mhz 200/5

    UART_config();
    UART_SendString("abc");
    ADC_config();
    while(1)
        {
                ADCProcessorTrigger(ADC0_BASE, 1);
                int i;
                for(i=0; i<10000;i++);
                //UART_SendInt(]);
                //ADCProcessorTrigger(ADC1_BASE, 3);
        }

}
*/
