/*
 * test.c
 *
 *  Created on: 10 thg 8, 2017
 *      Author: Kira Kira
 */
#include "test.h"
//volatile uint32_t Period;
//volatile uint32_t ui32PWMClock;



void PWM_config()
{
    Period= SysCtlClockGet()/20000;
    // Cau hinh tan so PWM
        SysCtlPWMClockSet(SYSCTL_PWMDIV_64);
        // Kich hoat chan PW1 F
        SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);
        //SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
        //GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);
        //Cau hinh PF0
        GPIOPinConfigure( GPIO_PF0_M1PWM4);
        //Cau hinh PWM
         PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_DOWN);
        PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, Period);    //Tao chu ky PWM
        PWMPulseWidthSet(PWM1_BASE,PWM_OUT_4,0);
        PWMOutputState(PWM1_BASE,PWM_OUT_4_BIT, true); //Xuat output ra PWM
        PWMGenEnable(PWM1_BASE, PWM_GEN_3);
        //Cau hinh chan PF1
        GPIOPinConfigure( GPIO_PF1_M1PWM5);  //Cau hinh chan PF0 la chan PWM
        //Cau hinh PWM
        PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_DOWN);
        PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, Period);    //Tao chu ky PWM
        PWMPulseWidthSet(PWM1_BASE,PWM_OUT_5,0);
        PWMOutputState(PWM1_BASE,PWM_OUT_5_BIT, true); //Xuat output ra PWM
        PWMGenEnable(PWM1_BASE, PWM_GEN_3); //Kich hoat PWM
        //Cau hinh PF2
        GPIOPinConfigure( GPIO_PF2_M1PWM6);
        //Cau hinh PWM
        PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_DOWN);
        PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, Period);    //Tao chu ky PWM
        PWMPulseWidthSet(PWM1_BASE,PWM_OUT_6,0);
        PWMOutputState(PWM1_BASE,PWM_OUT_6_BIT, true); //Xuat output ra PWM
        PWMGenEnable(PWM1_BASE, PWM_GEN_3); //Kich hoat PWM
        //Cau hinh PF3
        GPIOPinConfigure( GPIO_PF3_M1PWM7);
        //Cau hinh PWM
         PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_DOWN);
         PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, Period);    //Tao chu ky PWM
         PWMPulseWidthSet(PWM1_BASE,PWM_OUT_7,0);
         PWMOutputState(PWM1_BASE,PWM_OUT_7_BIT, true); //Xuat output ra PWM
         PWMGenEnable(PWM1_BASE, PWM_GEN_3);

    }

void Set_PWM( char id,uint32_t duty)
{
    uint32_t k;
    k = Period * duty/10-1;
    switch(id)
    {
    case 0:
        {
            PWMPulseWidthSet(PWM1_BASE, PWM_OUT_4, Period*duty/10-1); //Thuc hien PWM theo gia tri adj
            PWMOutputState(PWM1_BASE, PWM_OUT_4_BIT, true);
        } break;
    case 1:
        {
            PWMPulseWidthSet(PWM1_BASE, PWM_OUT_5, Period*duty/10-1); //Thuc hien PWM theo gia tri adj
            PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, true);
        }; break;
    case 2:
    {
        PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, Period*duty/10-1); //Thuc hien PWM theo gia tri adj
        PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, true);
    }; break;
    case 3:
        {
            PWMPulseWidthSet(PWM1_BASE, PWM_OUT_7, Period*duty/10-1); //Thuc hien PWM theo gia tri adj
            PWMOutputState(PWM1_BASE, PWM_OUT_7_BIT, true);
        }; break;
    default:break;
    }

}

