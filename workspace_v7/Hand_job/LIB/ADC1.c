/*
 * ADC1.c
 *
 *  Created on: 3 thg 10, 2017
 *      Author: Kira Kira
 */
//#include "../include.h"
#include "ADC1.h"

static void ADC_ISR(void);
extern void Config_ADC(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3 );

    ADCHardwareOversampleConfigure(ADC0_BASE,64);

    ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0);
    ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0|ADC_CTL_END|ADC_CTL_IE);
    ADCSequenceEnable(ADC0_BASE,1);

    ADCIntRegister(ADC0_BASE,1,&ADC_ISR);
    ADCIntEnable(ADC0_BASE,1);

}

static void ADC_ISR(void)
{
    ADCIntClear(ADC0_BASE,1);
    uint32_t Data[1];
    ADCSequenceDataGet(ADC0_BASE,1,(uint32_t *)&Data);
}





