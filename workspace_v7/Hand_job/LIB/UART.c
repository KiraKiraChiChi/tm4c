/*
 * Uart.c
 *
 *  Created on: 18-07-2017
 *      Author: Kira Kira
 */




#include "include.h"
#include "UART.h"



static char Buff[100];




#ifdef UART_NORMAL
void UARTGetBuffer(char *pBuff)
{
    static uint16_t i=0;
    char c;
    if(i==0)
        Reset_Buffer(pBuff);
    while(UARTCharsAvail(UART0_BASE))
    {
        c=UARTCharGet(UART0_BASE);
        *(pBuff+i)=c;
        i++;
    }
    if(c==0x0A)
        i=0;
}

static void UART_ISR(void)
{
    UARTIntClear(UART0_BASE,UARTIntStatus(UART0_BASE,true));
    UARTGetBuffer(&Buff[0]);
}
void Config_UART(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);

    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |UART_CONFIG_PAR_NONE);
    UARTIntRegister(UART0_BASE,&UART_ISR);
    IntEnable(INT_UART0);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
}

/*void UART_GetString(char *input)
{
    uint8_t i = 0;
    uint8_t temp;

    while(1)
    {
        temp = UARTgetc();
          if(temp == '\n' && i==0)
          {
               ;
          }
        else if(temp == 0x2E) //enter character
            break;
        else
        {
            input[i] = temp;
            i++;
        }
    }
    //i = 0;
}*/
void UARTPutBuffer(void)
{
    uint16_t i=0;
    while(Buff[i]!=0x0A)
    {
        UARTCharPut(UART0_BASE,Buff[i]);
        i++;
    }
}
#endif

#ifdef UART_BUFFERED

void Config_UART(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    UARTStdioConfig(0,115200, SysCtlClockGet());
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    UARTEchoSet(false);
}

void UARTPutBuffer(void)
{
    int16_t k;
    k=UARTgets(&Buff[0],100);
    while(UARTRxBytesAvail()!=0);
    UARTwrite(&Buff[0],k);
    Reset_Buffer(Buff);
    UARTFlushRx();
}


#else

int k;
static void UART_ISR(void)
{
    UARTIntClear(UART0_BASE,UARTIntStatus(UART0_BASE,true));
    //UARTGetBuffer(&Buff[0]);
    k=UARTgets(&Buff[0],100);
}

void Config_UART(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    UARTStdioConfig(0,115200, SysCtlClockGet());
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);

    UARTIntRegister(UART0_BASE,&UART_ISR);
    IntEnable(INT_UART0);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
}

void UART_SendChar(char c)
{
    UARTCharPut(UART0_BASE, c);
    if(UARTCharsAvail(UART0_BASE))
                UARTCharPut(UART0_BASE, UARTCharGet(UART0_BASE));
    }

void UART_SendString(char *c)
{
    while(*c){
        UART_SendChar(*c);
        c++;
      }
    }

void UARTPutBuffer(void)
{
    UARTwrite(&Buff[0],k);
}

#endif
