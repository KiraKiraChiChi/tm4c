/*
 * UART1.c
 *
 *  Created on: 14 thg 9, 2017
 *      Author: Kira Kira
 */
#include "include.h"

static char Buff[100];

void UART_config(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_SYSTEM);

    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 9600,UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |UART_CONFIG_PAR_NONE);
   // UARTIntRegister(UART0_BASE,&UART_ISR);
    //IntEnable(INT_UART0);
    //UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
}

/*void UARTGetBuffer(char *pBuff)
{
    static uint16_t i=0;
    char c;
    if(i==0)
        Reset_Buffer(pBuff);
    while(UARTCharsAvail(UART0_BASE))
    {
        c=UARTCharGet(UART0_BASE);
        *(pBuff+i)=c;
        i++;
    }
    if(c==0x0A)
        i=0;
}

static void UART_ISR(void)
{
    UARTIntClear(UART0_BASE,UARTIntStatus(UART0_BASE,true));
    UARTGetBuffer(&Buff[0]);
}
*/

/*void UART_GetString(char *input)
{
    uint8_t i = 0;
    uint8_t temp;

    while(1)
    {
        temp = UARTgetc();
          if(temp == '\n' && i==0)
          {
               ;
          }
        else if(temp == 0x2E) //enter character
            break;
        else
        {
            input[i] = temp;
            i++;
        }
    }
    //i = 0;
}*/

void UART_SendChar(char c)
{
    UARTCharPut(UART0_BASE, c);
    if(UARTCharsAvail(UART0_BASE))
    {
                UARTCharPut(UART0_BASE, UARTCharGet(UART0_BASE));
    }
}
void UART_SendString(char *c)
{
    while(*c){
        UART_SendChar(*c);
        c++;
      }
    }
void UART_SendInt(unsigned int n)
{
    unsigned char buffer[16];
       unsigned char i,j;

       if(n == 0) {
        UART_SendChar('0');
            return;
       }

       for (i = 15; i > 0 && n > 0; i--) {
            buffer[i] = (n%10)+'0';
            n /= 10;
       }

       for(j = i+1; j <= 15; j++) {
        UART_SendChar(buffer[j]);
       }
}
char UART_GetChar()
{
    char getChar;
    getChar = UARTCharGetNonBlocking(UART0_BASE);
    return(getChar);
    }
