/*
 * include.h
 *
 *  Created on: 21 thg 7, 2017
 *      Author: Kira Kira
 */

#ifndef LIB_INCLUDE_H_
#define LIB_INCLUDE_H_

//thu vien ho tro ve bool, int, string va toan hoc math
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

//thu vien driver API

#include "driverlib/gpio.h"
#include "driverlib/can.h"
#include "driverlib/eeprom.h"
#include "driverlib/i2c.h"
#include "driverlib/lcd.h"
//#include "driverlib/lcd16x2.h"
#include "driverlib/mpu.h"
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/timer.h"
#include "driverlib/adc.h"
#include "driverlib/interrupt.h"
#include "driverlib/qei.h"
#include "driverlib/fpu.h"
#include "driverlib/uart.h"
#include "driverlib/rom.h"

//thu vien ho tro phan cung
#include "inc/hw_ints.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

//thu vien khai bao them (dua vao cac module minh tao ra)
//#include "GPIO/GPIO.h"
//#include "ADC.h"
//#include "TIMER/Timer.h"
#include "UART1.h"
#include "ADC.h"
#include "ADC1.h"
#include "HandJob.h"
#include "uartstdio.h"
#include "ustdlib.h"
//#include "PWM.h"
#include "test.h"
//#include "delay.h"




#endif /* LIB_INCLUDE_H_ */
