/*
 * UART.h
 *
 *  Created on: 22 thg 7, 2017
 *      Author: Kira Kira
 */

#include "include.h"
#ifndef LIB_UART_H_
#define LIB_UART_H_

#define UART_FR_TXFF            0x00000020  // UART Transmit FIFO Full
#define UART_FR_RXFE            0x00000010  // UART Receive FIFO Empty
#define UART_LCRH_WLEN_8        0x00000060  // 8 bit word length
#define UART_LCRH_FEN           0x00000010  // UART Enable FIFOs
#define UART_CTL_UARTEN         0x00000001  // UART Enable
#define UART0_DR_R              (*((volatile unsigned long *)0x4000C000))
#define UART0_FR_R              (*((volatile unsigned long *)0x4000C018))

void Config_UART(void);
void UART_SendChar(char c);
void UART_SendString(char *c);
char UART_GetChar();
UART_GetString(char *input);
//void UART_GetString(char *bufPt, uint16_t max);
//static void Reset_Buffer(char *pBuff);
#endif /* LIB_UART_H_ */
