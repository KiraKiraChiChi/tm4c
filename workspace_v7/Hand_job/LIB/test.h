/*
 * test.h
 *
 *  Created on: 10 thg 8, 2017
 *      Author: Kira Kira
 */
#include "include.h"
#ifndef LIB_TEST_H_
#define LIB_TEST_H_
#define PWM_FREQUENCY 55 //Dinh nghia gia tri tan so pwm la 55
static uint32_t Period;


void PWM_config();
void Set_PWM(char id, uint32_t duty);

#endif /* LIB_TEST_H_ */
