/*
 * HandJob.h
 *
 *  Created on: 25 thg 7, 2017
 *      Author: Kira Kira
 */

#ifndef LIB_HANDJOB_H_
#define LIB_HANDJOB_H_



#define   NUMB_OF_MOTOR  4

typedef enum
{
     motor_a = 4,
     motor_b,
     motor_c,
     motor_d
}handjob_motor_id;

typedef struct
{
     char motor_angle[4];
     char motor_id;
}handjob_type;

void HJ_Init(handjob_type *hj);
handjob_type HJ_StringToHJ(char* input);




#endif /* LIB_HANDJOB_H_ */
