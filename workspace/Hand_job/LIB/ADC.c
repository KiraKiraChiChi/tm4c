/*
 * ADC.c
 *
 *  Created on: 29 thg 7, 2017
 *      Author: Kira Kira
 */



#include "ADC.h"
void ADC_config()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0); //Kich hoat ADC
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0); // cau hinh bat trigger ACD
    ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0);     //bat du lieu adc luu lan luot vao cac gia tri cua mang adc0value
    ADCSequenceStepConfigure(ADC0_BASE,1,1,ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE,1,2,ADC_CTL_CH2);
    ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH3 | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 1);    // kich hoat adc nhan du lieu
}

void ADC_GetValue(uint32_t *ADC)
{
    ADCIntClear(ADC0_BASE, 1);  //Xoa cac gia tri adc co tu truoc
    ADCProcessorTrigger(ADC0_BASE, 1);  //Thuc nhien trigger adc
    while (!ADCIntStatus(ADC0_BASE,1, false))   //Neu trang thai khoi tao cua ADC ok thi thuc hien buoc tiep theo
    {}
    ADCSequenceDataGet(ADC0_BASE,1 , ADC);    //Thuc hien lay du lieu va luu lai
    }



