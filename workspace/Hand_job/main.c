/*
 * main.c
 */
#include <stdint.h>
#include <stdbool.h>
#include "LIB/include.h"
void delay(unsigned int nCount);
int main(void)
 {
    uint32_t ui32ADC0Value[4]={0};  //Khai bao mang 4 phan tu chua gia tri doc duoc tu ADC
    char str[5] = {0};
    char str1[5] = {0};
    volatile uint32_t ui32Avg;
    volatile uint32_t ui32Avg1;

    SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ); //Cau hinh clock he thong
    Config_UART();
    UARTprintf("hello");
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0); //Kich hoat ADC
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    ADCSequenceConfigure(ADC0_BASE,1,ADC_TRIGGER_PROCESSOR,0); // cau hinh bat trigger ACD
    ADCSequenceStepConfigure(ADC0_BASE,1,0,ADC_CTL_CH0);     //bat du lieu adc luu lan luot vao cac gia tri cua mang adc0value
    ADCSequenceStepConfigure(ADC0_BASE,1,1,ADC_CTL_CH1);
    ADCSequenceStepConfigure(ADC0_BASE,1,2,ADC_CTL_CH2);
    ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH3 | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 1);    // kich hoat adc nhan du lieu
    //ADC_config();
    while(1)
    {
        //ADCIntClear(ADC0_BASE, 1);  //Xoa cac gia tri adc co tu truoc
        //ADCProcessorTrigger(ADC0_BASE, 1);  //Thuc nhien trigger adc
        //while (!ADCIntStatus(ADC0_BASE,1, false))   //Neu trang thai khoi tao cua ADC ok thi thuc hien buoc tiep theo
        //{}
        //ADCSequenceDataGet(ADC0_BASE,1 , ui32ADC0Value);    //Thuc hien lay du lieu va luu lai
        ADC_GetValue(ui32ADC0Value);
        ui32Avg = ui32ADC0Value[1]; //Tinh gia tri trung binh adc
        ui32Avg1 = ui32ADC0Value[0];
        sprintf(str,"%d\n",ui32Avg);
        UARTprintf(str);
        sprintf(str1,"%d\n",ui32Avg1);
        UARTprintf(str1);
        delay(100);
    }
}

void delay(unsigned int nCount)
{
    unsigned int i, j;

    for (i = 0; i < nCount; i++)
        for (j = 0; j < 0x2AFF; j++);
}


