/*
 * main.cpp
 *
 *  Created on: 21 thg 7, 2017
 *      Author: Kira Kira
 */
#include <Energia.h>

#if defined(PART_TM4C129XNCZAD)
#include "inc/tm4c129xnczad.h"
#elif defined(PART_TM4C1294NCPDT)
#include "inc/tm4c1294ncpdt.h"
#elif defined(PART_TM4C1233H6PM) || defined(PART_LM4F120H5QR)
#include "inc/tm4c123gh6pm.h"
#else
#error "**** No PART defined or unsupported PART ****"
#endif

#include <stdint.h>
#include <stdbool.h>
//#include "inc/hw_types.h"
//#include "inc/hw_memmap.h"
//#include "driverlib/sysctl.h"
//#include "driverlib/gpio.h"

#include "LIB/include.h"
#include "LIB/UART.h"
//Chuong trinh chinh


  int main(void) {

     UART_config();
     char *c;
    //Khai bao bien shift co nhiem vu dich bit
    uint8_t shift = GPIO_PIN_1;
    //Khoi tao clock he thong
    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN); //De hien ra nhac code ban co the an ctr+space
    //Kich hoat ngoai vi
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);    //Kich hoat port F
    // Cau hinh output xuat LED
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1 |GPIO_PIN_2 | GPIO_PIN_3); //O day su dung pin 1 2 3 cho 3 led RGB tren kit

    while(1)
    {
        UART_SendString("A1");
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, shift);
        SysCtlDelay(1000000);  //Delay
        //Dich bit sang trai voi y nghia bat LED xanh
        shift <<= 1;
        //Kiem tra neu LED xanh la sang thi quay tro lai LED do
        if(shift > GPIO_PIN_3)
            shift = GPIO_PIN_1;
        UART_GetString(c,16);
        UART_SendString(c);
        UART_SendString("\n");
    }
}




