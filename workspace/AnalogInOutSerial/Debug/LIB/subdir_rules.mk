################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
LIB/%.o: ../LIB/%.cpp $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/tools/arm-none-eabi-gcc/4.8.4-20140725/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fno-exceptions -Dprintf=iprintf -DF_CPU=80000000L -DENERGIA_EK-TM4C123GXL -DENERGIA_ARCH_TIVAC -DENERGIA=18 -DARDUINO=10610 -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/variants/EK-TM4C123GXL" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system/driverlib" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system/inc" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/cores/tivac" -I"C:/Users/Kira Kira/workspace_v7/AnalogInOutSerial" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/tools/arm-none-eabi-gcc/4.8.4-20140725/arm-none-eabi/include" -Os -ffunction-sections -fdata-sections -g -gdwarf-3 -gstrict-dwarf -w -Wall -fno-threadsafe-statics --param max-inline-insns-single=500 -mabi=aapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -fno-rtti -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

LIB/%.o: ../LIB/%.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Compiler'
	"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/tools/arm-none-eabi-gcc/4.8.4-20140725/bin/arm-none-eabi-gcc.exe" -c -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fno-exceptions -Dprintf=iprintf -DF_CPU=80000000L -DENERGIA_EK-TM4C123GXL -DENERGIA_ARCH_TIVAC -DENERGIA=18 -DARDUINO=10610 -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/variants/EK-TM4C123GXL" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system/driverlib" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system/inc" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/system" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/hardware/tivac/1.0.2/cores/tivac" -I"C:/Users/Kira Kira/workspace_v7/AnalogInOutSerial" -I"C:/Users/Kira Kira/AppData/Local/Energia15/packages/energia/tools/arm-none-eabi-gcc/4.8.4-20140725/arm-none-eabi/include" -Os -ffunction-sections -fdata-sections -g -gdwarf-3 -gstrict-dwarf -w -Wall -fno-threadsafe-statics --param max-inline-insns-single=500 -mabi=aapcs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o"$@" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


