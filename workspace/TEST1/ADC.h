/*
 * ADC.h
 *
 *  Created on: 23 thg 7, 2017
 *      Author: Kira Kira
 */
#include "include.h"
#ifndef LIB_ADC_H_
#define LIB_ADC_H_


void ADC_config(uint32_t *ADC);
void ADC_GetValue(uint32_t *ADC);



#endif /* LIB_ADC_H_ */
