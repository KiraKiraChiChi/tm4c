/*
 * ADC.c
 *
 *  Created on: 29 thg 7, 2017
 *      Author: Kira Kira
 */



#include "ADC.h"
void ADC_config(uint32_t *ADC)
{
    SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

      //Enable ADC0
      SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

      //Enable port E va  config cho cho cac chan E0->E4 ADC
      SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
      GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

      /*Config cho ADC sample sequencer */
      ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);


      ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH0);
      ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH1);
      ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH2);
      ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_CH3|ADC_CTL_IE|ADC_CTL_END);

      //Enable SS1
      ADCSequenceEnable(ADC0_BASE, 1);
}

void ADC_GetValue(uint32_t *ADC)
{
        //Xoa co ngat va kich hoat lay mau
        ADCIntClear(ADC0_BASE, 1);
        ADCProcessorTrigger(ADC0_BASE, 1);

        //lap cho den khi lay mau xong
        while(!ADCIntStatus(ADC0_BASE, 1, false));

        /*sau khi lay xong, gan vao ADC0_BASE de luu mau*/
        ADCSequenceDataGet(ADC0_BASE, 1, ADC);
        return (ADC);
    }




